var express = require('express');
var router = express.Router();
const resultsController = require('../controllers/resultsController');


router.get('/:n1/:n2', resultsController.sum);

router.post('/', resultsController.mult);

router.put('/', resultsController.div);

router.delete('/:n1/:n2', resultsController.rest);

module.exports = router;
