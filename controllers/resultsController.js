const express = require('express');

function sum(req, res, next){
  result = { result : parseInt(req.params.n1) + parseInt(req.params.n2) };
  res.json(result);
};

function mult(req, res, next){
  result = { result: parseInt(req.body.n1) * parseInt(req.body.n2) };
  res.json(result);
};

function div(req, res, next){
  result = { result: parseInt(req.body.n1) / parseInt(req.body.n2) };
  res.json(result);
};

function rest(req, res, next){
  result = { result : parseInt(req.params.n1) - parseInt(req.params.n2) };
  res.json(result);
};

module.exports = {
  sum,
  mult,
  div,
  rest
};
