const express = require('express');
const User = require('../models/user');
const { validationResult } = require('express-validator/check');

function create(req, res, next){
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }
  
  let user = new User({
    _name: req.body.name,
    _lastName: req.body.lastName,
    _age: req.body.age,
  });

  user.save()
      .then((obj) => {
        return res.status(200).json({
          errors:[],
          data: [obj],
        });
      })
      .catch((err) => {
        return res.status(500).json({
          errors: [{ message: 'Algo salio mal' }],
          data: [],
        })
      })

  // user.save((err, obj) => {
  //   err && res.send(err);
  //   res.send(`Crea un Usuario con nombre ${obj.fullName}`);
  // });
};

function list(req, res, next){
  // let name = req.params.name ? req.params.name : 'Sin nombre'
  // res.render('users/list', { name: req.params.name });
  let page = req.params.page ? req.params.page : 1
  const options = {
    page: page,
    limit: 1,
    select: '_name _lastName _age',
  };

  User.paginate({}, options, (err, result) => {
        // result.docs
        // result.totalDocs = 100
        // result.limit = 10
        // result.page = 1
        // result.totalPages = 10
        // result.hasNextPage = true
        // result.nextPage = 2
        // result.hasPrevPage = false
        // result.prevPage = null
      })
      // .select('_name _lastName _age')
      .then((objs) => {
        res.status(200).json({
          errors: [],
          data: [objs],
        });
      })
      .catch(() => {
        res.status(500).json({
          errors: [{ message: 'Algo salio mal'}],
          data: [],
        });
      });
};

function index(req, res, next){
  // res.send(`Lista un Usuario con ID: ${req.params.id}`);
  User.findById(req.params.id)
      .then((obj) => {
        res.status(200).json({
          errors: [],
          data: [obj],
        });
      })
      .catch((err) => {
        res.status(500).json({
          errors: [{ message: 'Algo salio mal'}],
          data: [],
        });
      });
};

function update(req, res, next){
  User.findById(req.params.id)
      .then((obj) => {
        obj.name = req.body.name ? req.body.name : obj.name;
        obj.lastName = req.body.lastName ? req.body.lastName : obj.lastName;
        obj.age = req.body.age ? req.body.age : obj.age;
        obj.save()
           .then((obj) => {
             res.status(200).json({
               errors: [],
               data: obj,
             });
           })
           .catch((err) => {
             res.status(500).json({
               errors: [{ message: 'Algo salio mal'}],
               data: [],
             });
           });
      })
      .catch((err) => {
        res.status(500).json({
          errors: [{ message: 'Algo salio mal'}],
          data: [],
        });
      });
};

function destroy(req, res, next){
  User.remove({_id: req.params.id})
      .then((obj) => {
        res.status(200).json({
          errors: [],
          data: obj,
        });
      })
      .catch((err) => {
        res.status(500).json({
          errors: [{ message: 'Algo salio mal'}],
          data: [],
        });
      })
};

module.exports = {
  create,
  list,
  index,
  update,
  destroy
};
